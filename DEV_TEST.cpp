// Dog Sitting App
//
//
// Author [Name] ([Email])
// Co-Author [Name] ([Email])
// Co-Author [Name] ([Email])
// Co-Author [Name] ([Email])
// Testing : Blake W Hausafus (bwhausafus@dmacc.edu)
//
// October 2023
//
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <main.cpp>
using namespace std;

int gobal_index = 0;
int gobal_index_fail = 0;

bool test_func_int(int X, int Y){
    if (X == Y){
        gobal_index ++;
        return true;
    }
    else{
        gobal_index ++;
        gobal_index_fail ++;
        cout << "TEST " << gobal_index << " FAILED!" << endl;
        return false;
    }
};

bool test_func_string(string X, string Y){
    if (X == Y){
        gobal_index ++;
        return true;
    }
    else{
        gobal_index ++;
        gobal_index_fail ++;
        cout << "TEST " << gobal_index << " FAILED!" << endl;
        return false;
    }
};

bool test_func_bool(bool X, bool Y){
    if (X == Y){
        gobal_index ++;
        return true;
    }
    else{
        gobal_index ++;
        gobal_index_fail ++;
        cout << "TEST " << gobal_index << " FAILED!" << endl;
        return false;
    }
};


void dev_test() {

    vector<string> TEST_STRING_0;
    vector<string> TEST_STRING_1;

    vector<int> TEST_INTEGER_A;
    vector<int> TEST_INTEGER_B;

    TEST_STRING_0.push_back("???");
    TEST_STRING_0.push_back("???");

    TEST_STRING_1.push_back("Bichon Frisé");
    TEST_STRING_1.push_back("Labrador Retriever");


    for(int i=0; i < 10; i++){
        TEST_INTEGER_A.push_back(i);
    };

    for(int i=0; i < 30; i++){
        TEST_INTEGER_B.push_back(i);
    };

   dog dog_A("Duff", "Bichon Frisé", 5, 16, false, false, true);
   dog dog_B("Remi", "Labrador Retriever", 2, 79, true, true, true);
   dogSitter dogSitter_A("Bill", "Overbeck", TEST_STRING_0, 1 , 10, 12, 30, false, false, true, TEST_INTEGER_A, TEST_INTEGER_B);
   dogSitter dogSitter_B("Blake", "Hausafus", TEST_STRING_1, 2 , 7, 20, 80, true, true, false, TEST_INTEGER_A, TEST_INTEGER_B);
   client client_A  ("Tiffany", "2006 S Ankeny Blvd, Ankeny, IA", "(515) 964-6200", true);
   client client_B  ("Izabelle", "5959 Grand Ave, West Des Moines, IA", "(515) 633-2407", false);
   job Job_A  (dogSitter_A, client_A, "7:30", "18:00", 400);
   job Job_B  (dogSitter_A, client_A, "6:00", "16:00", 600);



    cout << test_func_string(dog_A.getDogName(), "Duff") << endl;
    cout << test_func_string(dog_A.getDogBreed(), "Bichon Frisé") << endl;
    cout << test_func_int(dog_A.getDogAge(), 5) << endl;
    cout << test_func_int(dog_A.getDogWeight(), 16) << endl;
    cout << test_func_bool(dog_A.isDogHyper(), false) << endl;
    cout << test_func_bool(dog_A.doesDogBite(), false) << endl;
    cout << test_func_bool(dog_A.doesDogHaveAnxiety(), true) << endl;

    cout << test_func_string(dog_B.getDogName(), "Remi") << endl;
    cout << test_func_string(dog_B.getDogBreed(), "Labrador Retriever") << endl;
    cout << test_func_int(dog_B.getDogAge(), 2) << endl;
    cout << test_func_int(dog_B.getDogWeight(), 79) << endl;
    cout << test_func_bool(dog_B.isDogHyper(), true) << endl;
    cout << test_func_bool(dog_B.doesDogBite(), true) << endl;
    cout << test_func_bool(dog_B.doesDogHaveAnxiety(), true) << endl;

    cout << test_func_string(dogSitter_A.getFullName(), "Bill Overbeck") << endl;
    cout << test_func_int(dogSitter_A.getMinAge(), 1) << endl;
    cout << test_func_int(dogSitter_A.getMaxAge(), 10) << endl;
    cout << test_func_int(dogSitter_A.getMinWeight(), 12) << endl;
    cout << test_func_int(dogSitter_A.getMaxWeight(), 30) << endl;
    cout << test_func_bool(dogSitter_A.getHandlesHyper(), false) << endl;
    cout << test_func_bool(dogSitter_A.getHandlesBites(), false) << endl;
    cout << test_func_bool(dogSitter_A.getHandlesAnxiety(), true) << endl;

    cout << test_func_string(dogSitter_B.getFullName(), "Blake Hausafus") << endl;
    cout << test_func_int(dogSitter_B.getMinAge(), 2) << endl;
    cout << test_func_int(dogSitter_B.getMaxAge(), 7) << endl;
    cout << test_func_int(dogSitter_B.getMinWeight(), 20) << endl;
    cout << test_func_int(dogSitter_B.getMaxWeight(), 80) << endl;
    cout << test_func_bool(dogSitter_B.getHandlesHyper(), true) << endl;
    cout << test_func_bool(dogSitter_B.getHandlesBites(), true) << endl;
    cout << test_func_bool(dogSitter_B.getHandlesAnxiety(), false) << endl;

    cout << test_func_int(compareSitters(dogSitter_B, dogSitter_A, dog_B), true)<< endl;
    cout << test_func_int(compareSitters(dogSitter_B, dogSitter_A, dog_A), false)<< endl;


    cout << test_func_string(Job_A.getDropOffTime(), "7:30") << endl;
    cout << test_func_string(Job_A.getEstimatedPickUpTime(), "18:00") << endl;
    cout << test_func_int(Job_A.getQuote(), 400) << endl;


    cout << test_func_string(Job_B.getDropOffTime(), "6:00") << endl;
    cout << test_func_string(Job_B.getEstimatedPickUpTime(), "16:00") << endl;
    cout << test_func_int(Job_B.getQuote(), 600) << endl;

    cout << gobal_index <<  " tests, " << gobal_index_fail << " fails" << endl;

    //  dog_A.displayDogInfo();

    //  dog_B.displayDogInfo();

    //    cout << test_func_string(client_A.getClientName(), "Tiffany") << endl;
    //    cout << test_func_string(client_A.getAddress(), "2006 S Ankeny Blvd, Ankeny, IA") << endl;
    //    cout << test_func_string(client_A.getPhoneNum(), "(515) 964-6200") << endl;

    //    cout << test_func_string(client_B.getClientName(), "Izabelle") << endl;
    //    cout << test_func_string(client_B.getAddress(), "5959 Grand Ave, West Des Moines, IA") << endl;
    //    cout << test_func_string(client_B.getPhoneNum(), "(515) 633-2407") << endl;

};
